#ifndef __DLT645_2007_H
#define __DLT645_2007_H

#include "dlt645.h"

//基于 DLT645 2007 协议读取数据
int dlt645_2007_read_data(dlt645_t *ctx, uint32_t code, uint8_t *read_data);
int dlt645_2007_cmd_send(dlt645_t *ctx, uint8_t *write_data,uint8_t write_len);
//基于 DLT645 2007 协议校验数据
int dlt645_2007_recv_check(uint8_t *msg, int len, uint8_t *addr, uint32_t code);
int dlt645_2007_recv_feedback_check(uint8_t *msg, int len, uint8_t * addr);

#endif /* __DLT645_2007_H */
