
#include "ammeter.h"
#include <stdlib.h>
#include <stdio.h>
#include "math.h"

int main(void)
{
	unsigned char HoldAmmeterReadResult = 0x00;
    tsAmmeterDataType AmmeterData;
    int i;
    unsigned int dataPow;

    ammeter_Init();

    HoldAmmeterReadResult = ammeter_ReadParameters(&AmmeterData);

    if (HoldAmmeterReadResult == 0x00)
    {
        printf("\r\nammeter data\r\n---------------------------\r\n");
        printf("::Power     :%f\r\n", AmmeterData.TotoalPower);
        printf("::Voltage   :%f\r\n", AmmeterData.Voltage);
        printf("::Current   :%f\r\n", AmmeterData.Current);
        printf("::PowerCoef :%f\r\n", AmmeterData.PowerCoef);
    }
    else
    {
        printf("\r\nAmmeter Read Error :%02x\r\n",HoldAmmeterReadResult);
    }

    for (i = 0; i <= 10; i++)
    {
        dataPow = pow(10, i);
        printf("\r\n%02d: %u", i, dataPow);
    }

	return 0;
}
