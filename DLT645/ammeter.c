#include "dlt645.h"
#include "dlt645_port.h"
#include "ammeter.h"

#define AMMETER_CMD_READ 0x01


#define AMMETER_CMD_OPS  0x02

unsigned char gcAmmeterCmdFlag = AMMETER_CMD_READ;

uint8_t test_addr[6] = {0x20, 0x20, 0x06, 0x00, 0x06, 0x16};
#if 0
#define DLT645_2007_READ_TOTAL_POWER      0x02030000
#define DLT645_2007_READ_CURRENT_POWER    0x02030000
#define DLT645_2007_READ_VOLTAGE          0x02010100
#define DLT645_2007_READ_CURRENT          0x02020100
#define DLT645_2007_READ_POWERCOEF        0x02060000
#else
#define DLT645_2007_READ_TOTAL_POWER      0x02030000
#define DLT645_2007_READ_CURRENT_POWER    0x02030000
#define DLT645_2007_READ_VOLTAGE          0x02020100
#define DLT645_2007_READ_CURRENT          0x02010100
#define DLT645_2007_READ_POWERCOEF        0x02060000
#endif
int ammeter_ReadParameters(tsAmmeterDataType *AmmmeterData)
{
    unsigned char read_buf[4];
    unsigned char rst = 0x00;    

    
    if(dlt645_read_data(&dlt645,DLT645_2007_READ_TOTAL_POWER,read_buf,DLT645_2007) > 0)  //2007采集测试
    {
        AmmmeterData->TotoalPower = *(float *)read_buf;      
        rst &= ~(1 << 0);  
    }
    else
    {
        rst |= 1 << 0;
    }
#if 0
    if(dlt645_read_data(&dlt645,DLT645_2007_READ_CURRENT_POWER,read_buf,DLT645_2007) > 0)  //2007采集测试
    {
        AmmmeterData->CurrentPower = *(float *)read_buf;    
        rst &= ~(1 << 1);    
    }
    else
    {
        rst |= 1 << 1;
    }
#endif

    if(dlt645_read_data(&dlt645,DLT645_2007_READ_VOLTAGE,read_buf,DLT645_2007) > 0)  //2007采集测试
    {
        AmmmeterData->Voltage = *(float *)read_buf;  
        rst &= ~(1 << 2);      
    }
    else
    {
        rst |= 1 << 2;
    }

    if(dlt645_read_data(&dlt645,DLT645_2007_READ_CURRENT,read_buf,DLT645_2007) > 0)  //2007采集测试
    {
        AmmmeterData->Current = *(float *)read_buf;    
        rst &= ~(1 << 3);    
    }
    else
    {
        rst |= 1 << 3;
    }


    if(dlt645_read_data(&dlt645,DLT645_2007_READ_POWERCOEF,read_buf,DLT645_2007) > 0)  //2007采集测试
    {
        AmmmeterData->PowerCoef = *(float *)read_buf;      
        rst &= ~(1 << 4);  
    }
    else
    {
        rst |= 1 << 4;
    }

    return rst;
}


unsigned char gAmmeterPowerCmdPowerOn[12] = {0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1a, 0x00, 0x00, 0x00};
unsigned char gAmmeterPowerCmdPowerOff[12] = {0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1b, 0x00, 0x00, 0x00};
int ammeter_PowerOn(void)
{
    return  dlt645_2007_cmd_send(&dlt645, gAmmeterPowerCmdPowerOn,12);
}

int ammeter_PowerOff(void)
{
    return  dlt645_2007_cmd_send(&dlt645, gAmmeterPowerCmdPowerOff,12);
}

void ammeter_Init(void)
{
    dlt645_set_addr(&dlt645,test_addr);
    dlt645_port_init();
}


