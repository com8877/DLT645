/*ammeter.h*/
#ifndef _AMMETER_H_
#define _AMMETER_H_

typedef struct _tsAmmeterDataType
{
    float TotoalPower;
    float CurrentPower;
    float Voltage;
    float Current;
    float PowerCoef;
}tsAmmeterDataType;

int ammeter_ReadParameters(tsAmmeterDataType *AmmmeterData);
void ammeter_Init(void);
int ammeter_PowerOn(void);
int ammeter_PowerOff(void);
#endif 
